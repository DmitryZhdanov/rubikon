import { debug } from './debug';
import { ResizeService } from './resize';
import 'velocity-animate';
import 'velocity-ui-pack';
// import 'velocity-animate/velocity.ui.min.js';

exports.Nav = {
	bindHandlers: () => {
		function updateMenu(submenu, submenuContainer, windowWidth, aItem) {
			submenu.css({
				left: ((menuWidth, linkWidth) => {
					return -(menuWidth - linkWidth) / 2;
				})(submenuContainer.outerWidth(), aItem.outerWidth())
			});

			(submenu.offset().left + submenuContainer.outerWidth() > windowWidth) 
				? ( submenu.css({
					left: submenu.position().left - ((submenu.offset().left + submenuContainer.outerWidth()) - windowWidth)
				}) ) : 
			( submenu.offset().left < 0 )
				? ( submenu.css({
					left: -(submenu.offset().left - submenu.position().left)
				}) ): null;
		}

		ResizeService.subscribe((size) => {
			$('.nav-submenu-container:not(.hidden)').each(function() {
				updateMenu($(this).parent(), $(this), size.width, $(this).parents('.nav-item').children('a'));
			});
		});

		$('.nav-item')
			.on({
				mouseenter: function(e) {
					if( !$('.nav-submenu', this).length ) return;	

					let submenu = $('.nav-submenu', this),
						submenuContainer = $('.nav-submenu-container', this),
						windowWidth = $(window).outerWidth(),
						aItem = $('> a', this);
					
					updateMenu(submenu, submenuContainer, windowWidth, aItem);

					submenuContainer.removeClass('hidden');

					$('.nav-submenu-item', this)
						.velocity('stop')
						.velocity('transition.slideDownIn', { 
							stagger: 100,
							duration: 200,
							drag: true
						 });
				},
				mouseleave: function(e) {
					$('.nav-submenu-item', this)
						.velocity('stop');
					$('.nav-submenu-container', this).addClass('hidden');
				}
			})
	}
}