import { debug } from './debug';
import { common } from './common';

exports.ResizeService = (function(){
	let module = {},
		$w = $(window);

	module.subscribers = [];
	module.subscribe = (callback) => module.subscribers.push(callback);
	module.get = () => ({
		width: $w.outerWidth(),
		height: $w.outerHeight()
	});
	
	$w
		.on({
			resize: common.debounce(() => _.each(module.subscribers, (c) => c(module.get())), 250)
		});

	return module;
}());