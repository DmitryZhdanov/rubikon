import { createElement } from './element.js';
import { Modal } from './modal.js';

exports.Interaction = {
	initHandlers: function(options) {
		let menuIsOpen = false;

		$(document).on({
			click: function(e) {
				e.preventDefault();
				let $menu = $(this).siblings('.container');
				let $body = $('body');

				menuIsOpen = $menu.hasClass('nav-show');

				$menu.toggleClass('nav-show');

				if(menuIsOpen) {
					$body.removeClass('no-scroll');
					$('.nav-overlay')
						.fadeOut(function() {
							$(this).remove();
						});
				} else {
					var $overlay = createElement('nav-overlay');
					$body
						.addClass('no-scroll');

					$overlay
						.appendTo($body)
						.hide()
						.fadeIn(400);
				}

			}
		}, '.nav-toogle');

		$(document).on({
			click: function(e) {
				e.preventDefault();
				$('.nav-toogle').trigger('click');
			}
		}, '.nav-overlay, .nav-close');

		let headerHeight = $('.header').outerHeight(),
			navHeight = $('.nav').outerHeight();

		// To fixed header
		$(window).on({
			scroll: function(e) {
				if($(window).scrollTop() > headerHeight) {
					$('.nav').addClass('nav-fixed');
					if(!$('.nav-fallback').length)
						$('.header').after(createElement('nav-fallback'));
				} else {
					$('.nav').removeClass('nav-fixed');
					$('.nav-fallback').remove();
				}
			}
		});


		// Modals

		$(document)
			.on({
				click: function(e) {
					e.preventDefault();
					Modal.open($(this).data('modal'));
				}
			}, '[data-modal]');


		

	}
};