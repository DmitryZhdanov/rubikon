import { notif } from 'notifIt/js/notifIt';
import 'notifIt/css/notifIt.css';
import _ from 'lodash';

let types = {
	custom: {
		position: 'center',
		opacity: 0.9,
		bgcolor: '#fafafa',
		color: '#666'
	}
}

exports.Notify = (type, options) => {
	_.extend(options, types[type]);
	notif(options);
}