exports.debug = {
    log: function(...args) {
        if(window.console && ENV === 'development') {
            console.log(...args);
        }
    }
}