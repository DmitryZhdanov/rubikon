const collection = {
    'nav-overlay': () => $('<div class="nav-overlay"></div>'),
    'nav-fallback': () => $('<div class="nav-fallback"></div>'),
    'dialog-overlay': () => $('<div id="dialog-overlay">'),
    'dialog-subscribe': () => $($('#dialog-subscribe').html()),
    'dialog-callme': () => $($('#dialog-callme').html())
}

exports.createElement = function(name) {
    return collection[name]();
}