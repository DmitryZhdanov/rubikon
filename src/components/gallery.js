import $ from 'jquery';
import imagesLoaded from 'imagesloaded';
import Masonry from 'masonry-layout';
import { ResizeService } from './resize.js';

import './plugins/jquery.fancybox';
import './plugins/jquery.fancybox.css';

$.fancybox.defaults.lang ='ru';
$.fancybox.defaults.i18n = {
	'ru' : {
		CLOSE       : 'Закрыть',
		NEXT        : 'Вперед',
		PREV        : 'Назад',
		ERROR       : 'Произошла ошибка загрузки, повторите позже.',
		PLAY_START  : 'Начать слайдшоу',
		PLAY_STOP   : 'Остановить слайдшоу',
		FULL_SCREEN : 'Полноэкранный режим',
		THUMBS      : 'Все элементы'
	}
}

$(document).on({
	click: function() {
		$(this).fancybox({
			buttons : [
				'slideShow',
				'thumbs',
				'close'
			]
		});
	}
}, '#openVideo');

const gallery = {
	msnry: null,
	init: function(options) {
		new imagesLoaded( $('.gallery').get(0), () => {
			gallery.msnry = new Masonry( '.gallery', {
				itemSelector: '.gallery-item',
				columnWidth: '.gallery-item',
				// gutter: 10,
				horizontalOrder: true,
				// fitWidth: true
			});
			
			$('.gallery-loading').fadeOut(function() {
				$(this).remove();
			});
			$('.gallery').removeClass('invisible');

			gallery.fadeUpImages();
			if(options.complete)
				options.complete();
		});


		// ResizeService.subscribe((size) => {
		// 	$('.lightbox-item').each(function() {
		// 		$(this)
		// 			.css({
		// 				left: (size.width / 2) - ($(this).outerWidth() / 2),
		// 				top: (size.height / 2) - ($(this).outerHeight() / 2)
		// 			});
		// 	});
		// });
	},
	fadeUpImages: () => {
		$('.gallery-item').hide()
			.velocity('stop')
			.velocity('transition.fadeIn', { 
				duration: 300,
				stagger: 60,
				drag: true
			});
	}
}

//tabs
$(document)
	.on({
		click: function(e) {
			let targetLink = $(this);
			let target = $('[data-tab="'+ $(this).data('target') +'"]');

			$('.tabs-link[data-visible]').each(function() {
				$(this).toggle($(this).data('visible') === targetLink.data('target'));
			});

			$('.tabs-item-active')
				.velocity('stop')
				.velocity('transition.slideRightOut', { 
					duration: 200,
					complete: function() {
						$(this).removeClass('tabs-item-active');
						$('.tabs-link-active').removeClass('tabs-link-active');
						targetLink.addClass('tabs-link-active');
						target
							.addClass('tabs-item-active')
							.velocity('stop')
							.velocity('transition.slideLeftIn', { 
								duration: 200,
								complete: function() {
									$(this).removeClass('tabs-item-active');
									$('.tabs-link-active').removeClass('tabs-link-active');
									targetLink.addClass('tabs-link-active');
									target.addClass('tabs-item-active');
								}
							});
					}
				});
		}
	}, '.tabs-link:not(.tabs-link-active):not([data-visible])');

exports.Gallery = gallery;