exports.Map = {
    init: function() {
        ymaps.ready(_init);

        function _init() {
            var myMap = new ymaps.Map("map", {
                    center: [55.75, 37.716297],
                    zoom: 15,
                    controls: ['zoomControl']
                }, {
                    searchControlProvider: 'yandex#search'
                });

            myMap.behaviors.disable('scrollZoom'); 

            myMap.geoObjects
                .add(new ymaps.Placemark([55.750899, 37.717596], {
                   iconCaption: 'Метро',
                    iconCaption: 'Метро'
                }, {
                    preset: 'islands#redDotIconWithCaption'
                }))
                .add(new ymaps.Placemark([55.747723, 37.716297], {
                    iconCaption: 'Батутный клуб Рубикон'
                }, {
                    preset: 'islands#blueDotIconWithCaption'
                }));

            myMap.container.fitToViewport();
        }
    }
}