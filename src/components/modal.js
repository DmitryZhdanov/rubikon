import { createElement } from './element';
import { Notify } from './notify';

let module = {
	open: (type) => {
		createElement('dialog-overlay')
			.appendTo($('body'))
			.hide()
			.fadeIn();

		let $dialog = createElement('dialog-' + type);

		$dialog
			.appendTo($('body'))
			.css({
				left: ($(window).width() / 2) - ($dialog.outerWidth() / 2),
				top: ($(window).height() / 2) - ($dialog.outerHeight() / 2)
			})
			.hide()
			.velocity('stop')
			.velocity('transition.slideDownIn', { 
				duration: 400
			});

		$('[data-type="time"]', $dialog).each(function() {
			
		})
	},
	close: () => {
		$('#dialog-overlay')
			.fadeOut(function() {
				$(this).remove();
			});

		$('.dialog')
			.each(function() {
				$(this)
				.velocity('stop')
				.velocity('transition.slideUpOut', { 
					duration: 400,
					complete:function() {
						$(this).remove();
					}
				});
			});
	}
};

const modalTypes = {
	subscribe: {
		submit: ($modal) => {
			let hasErrors = false;
			let data = {};
			$('.input-group', $modal).each(function() {
				data[$('.form-control', this).prop('name')] = $('.form-control', this).val();
				let $el = $('[required].form-control', this);
				if(!$el.length) return;
				let currentErr = !$.trim($el.val()).length;
				if(!hasErrors) {
					hasErrors = currentErr;
				}
				$(this).toggleClass('has-warning', currentErr);
			});

			if(hasErrors) return;

			$modal.block();

			setTimeout(()=> {
				$.ajax({
					type: 'POST',
					url: '/zo.php',
					data: data,
					success: (data) => {
						Notify('custom', {
							msg: '<i class="fa fa-bell"></i> ' + data.message,
						});
					},
					error: () => Notify('custom', {
						msg: 'Произошла ошибка запроса, повторите позже!',
					}),
					complete: () => {
						$modal.unblock();
						module.close();
					}
				})
			}, 500);
		}
	},
	callme: {
		submit: ($modal) => {
			let hasErrors = false;
			let data = {};
			$('.input-group', $modal).each(function() {
				data[$('.form-control', this).prop('name')] = $('.form-control', this).val();
				let $el = $('[required].form-control', this);
				if(!$el.length) return;
				let currentErr = !$.trim($el.val()).length;
				if(!hasErrors) {
					hasErrors = currentErr;
				}
				$(this).toggleClass('has-warning', currentErr);
			});

			if(hasErrors) return;

			$modal.block();

			setTimeout(()=> {
				$.ajax({
					type: 'POST',
					url: '/zz.php',
					data: data,
					success: (data) => {
						Notify('custom', {
							msg: '<i class="fa fa-bell"></i> ' + data.message,
						});
					},
					error: () => Notify('custom', {
						msg: 'Произошла ошибка запроса, повторите позже!',
					}),
					complete: () => {
						$modal.unblock();
						module.close();
					}
				})
			}, 500);
		}
	}
}

$(document)
	.on({
		click: function(e) {
			e.preventDefault();
			module.close();
		}
	}, '#dialog-overlay, .dialog-close');

$(document)
	.on({
		click: function(e) {
			e.preventDefault();
			modalTypes
				[$(this).parents('[data-dialog-type]').data('dialog-type')]
				[$(this).data('dialog-action')]($(this).parents('.dialog-body'));
		}
	}, '[data-dialog-action]');

exports.Modal = module;