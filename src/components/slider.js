import 'skippr/dist/skippr.js';
import 'skippr/dist/skippr.css';

exports.Slider = {
    create: function($el) {
        $el.skippr({
            transition: 'slide', // slide|fade
            speed: 1000,
            easing: 'easeOut',
            navType: 'bubble', // block, bubble	
            childrenElementType: 'div',
            arrows: true,
            autoPlay: true,
            autoPlayDuration: 7000,
            keyboardOnAlways: true,
            hidePrevious: false
        });
    }
}