import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import $ from 'jquery';
import 'imports-loader?jQuery=jquery!owl.carousel';
import 'animate.css/animate.min.css';

exports.Owl = {
    create: function($el) {
        $el.owlCarousel({
            animateIn: 'zoomIn',
            animateOut: 'zoomOut',
            margin: 10,
            nav: false,
            rewind: true,
            dotsEach: true,
            // autoplay: true,
            autoplayTimeout: 5000,
            smartSpeed: 800,
            navSpeed: 400,
            dotsSpeed: 350,
            responsive: {
                0: {
                    items: 1
                },
                1000: {
                    items: 3
                }
            },
            navText: [
                '<i class="fa fa-chevron-left"></i>',
                '<i class="fa fa-chevron-right"></i>'
            ]
        });
    }
}