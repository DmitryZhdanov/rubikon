import _ from 'lodash';
import $ from 'jquery';
import Pace from 'imports-loader?define=>false!pace-progress';
import 'pace-progress/themes/yellow/pace-theme-flash.css';

import { Slider } from 'components/slider';
import { Interaction } from 'components/interaction';
import { Map } from 'components/map';
import { Owl } from 'components/owl';
import { debug } from 'components/debug';
import { Scrollbars } from 'components/scrollbars';
import { Gallery } from 'components/gallery';
import { Nav } from 'components/nav';

import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';

// Import base
import 'styles/main.scss';

// Temp import for watching
import 'html-loader!./index.ejs';
import 'templates/gallery/gallery-noheader.html';

import 'block-ui';
$.blockUI.defaults.css = {}; 
$.blockUI.defaults.blockMsgClass = 'block-overlay';
$.blockUI.defaults.message = '<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
$.blockUI.defaults.overlayCSS = { 
	backgroundColor: '#666', 
	opacity:         0.6, 
	cursor:          'wait' 
};

(() => {
	Pace.go();

	$(document).ready(function() {
		setTimeout(()=> {
			try{
				$('#preloader')
					.fadeOut(function() {
						$(this).remove();
					});
				Nav.bindHandlers();
				Interaction.initHandlers();
				Owl.create($('.owl-carousel'));
			} catch(e) {
				throw new Error('Unexpected error! ', e);
			}
		}, 500);
	});
})();