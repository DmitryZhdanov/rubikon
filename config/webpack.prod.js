const path = require('path');
const CommonConfig = require('./webpack.common.js');
const ENV = process.env.ENV = process.env.NODE_ENV = 'production';
const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const DefinePlugin = require('webpack/lib/DefinePlugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function(options) {
	return webpackMerge(CommonConfig(), {
		module: {
			rules: [
				{
					test: /\.scss$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: ['css-loader', 'sass-loader']
					})
				}, 
				{
					test: /\.css$/,
					use: ExtractTextPlugin.extract({
						fallback: 'style-loader',
						use: ['css-loader']
					})
				}]
		},
		plugins: [
			new CleanWebpackPlugin(['dist'], {
				root: path.resolve(__dirname, '../'),
				verbose: true,
				dry: false
			}),
			new ExtractTextPlugin({
				filename: '[name].styles.css'
			}),
			// new OptimizeCssAssetsPlugin({
			// 	cssProcessorOptions: { 
			// 		discardComments: {
			// 			removeAll: true 
			// 		} 
			// 	},
			// 	cssProcessor: require('cssnano'),
			// 	canPrint: true
			// }),
			new DefinePlugin({
				'ENV': JSON.stringify(ENV),
				'process.env': {
					'ENV': JSON.stringify(ENV),
					'NODE_ENV': JSON.stringify(ENV)
				}
			})]
	})
}

