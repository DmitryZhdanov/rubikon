const path = require('path');
const Autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProvidePlugin = require('webpack').ProvidePlugin;
const Webpack = require('webpack');

process.stdout.write("Build in progress \n");

module.exports = function(options) {
	return {
		entry: {
			main: 'pages/home/index.js',
			programm: 'pages/programm/index.js',
			trainers: 'pages/trainers/index.js',
			price: 'pages/price/index.js',
			about: 'pages/about/index.js',
			common: ['components/common']
		},	
		output: {
			filename: '[name].bundle.js',
			path: path.resolve(__dirname, '../dist')
		},
		resolve: {
			alias: {
				notifIt: path.resolve(__dirname, '../bower_components/notifIt/notifIt'),
				components: path.resolve(__dirname, '../src/components/'),
				templates: path.resolve(__dirname, '../src/templates/'),
				styles: path.resolve(__dirname, '../src/styles/'),
				pages: path.resolve(__dirname, '../src/pages/')
			}
		},
		module: {
			rules: [{
				test: /\.json$/,
				use: 'json-loader'
			}, {
				test: /\.html$/,
				use: ['html-loader'],
			}, {
				test: /\.pug$/,
				loader: 'pug-loader'
			}, { 
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['es2015']
					}
				}
			}, {
					test: /\.(jpe?g|png|gif)$/,
					loader: 'url-loader?limit=10000'
			}, {
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: "url-loader?limit=10000&name=./fonts/[hash].[ext]"
			}, {
					test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: "file-loader?name=[hash].[ext]&outputPath=./fonts/"
			}]
		},
		plugins: [
			new HtmlWebpackPlugin({
					template: path.resolve(__dirname, '../src/pages/home/index.ejs'),
					filename: 'index.html',
					title: 'Главная страница',
					chunks: [
						'main',
						'common'
					],
					hash: true,
					// minify: {
					// 	collapseWhitespace: true
					// }
			}),
			new HtmlWebpackPlugin({
					template: path.resolve(__dirname, '../src/pages/programm/index.ejs'),
					filename: 'programm.html',
					title: 'Программа батута',
					chunks: [
						'programm',
						'common'
					],
					hash: true,
			}),
			new HtmlWebpackPlugin({
					template: path.resolve(__dirname, '../src/pages/trainers/index.ejs'),
					filename: 'trainers.html',
					title: 'Тренеры',
					chunks: [
						'trainers',
						'common'
					],
					hash: true,
			}),
			new HtmlWebpackPlugin({
					template: path.resolve(__dirname, '../src/pages/price/index.ejs'),
					filename: 'price.html',
					title: 'Цены',
					chunks: [
						'price',
						'common'
					],
					hash: true,
			}),
			new HtmlWebpackPlugin({
					template: path.resolve(__dirname, '../src/pages/about/index.ejs'),
					filename: 'about.html',
					title: 'О нас',
					chunks: [
						'about',
						'common'
					],
					hash: true,
			}),
			new Webpack.optimize.CommonsChunkPlugin({
				name: 'common',
				minChunks: 2,
				// chunks: [],
			}),
			new CopyWebpackPlugin([{
				from: 'src/assets',
				to: 'assets'
			}]),
			new ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery',
				"window.jQuery": 'jquery',
				"windows.jQuery": 'jquery',
			}),
		]
	}
};