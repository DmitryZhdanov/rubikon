const path = require('path');
const CommonConfig = require('./webpack.common.js');
const ENV = process.env.ENV = process.env.NODE_ENV = 'development';
const webpackMerge = require('webpack-merge');
const DefinePlugin = require('webpack/lib/DefinePlugin');

module.exports = function(options) {
	return webpackMerge(CommonConfig(), {
		module: {
			rules: [{
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', {
						loader: 'sass-loader',
						options: {
							sourceMap: true,
							includePaths: ['src/styles']
						}
				}]
			}, {
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			}]
		},
		plugins: [
			new DefinePlugin({
				'ENV': JSON.stringify(ENV),
				'process.env': {
					'ENV': JSON.stringify(ENV),
					'NODE_ENV': JSON.stringify(ENV)
				}
			})
		]
	});
}


